package com.parth.jtcompose_demo.Classes

sealed class Screen(val route : String){
    object MainScreen : Screen("main_screen")
    object DetailScreen : Screen("details_screen")

    fun _withArgs(vararg args : String) : String{
        return buildString {
            append(route)
            args.forEach {
                append(("/$it"))
            }
        }
    }
}
