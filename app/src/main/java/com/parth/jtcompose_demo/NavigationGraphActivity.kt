package com.parth.jtcompose_demo

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.parth.jtcompose_demo.ui.theme.JtCompose_DemoTheme
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Surface
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.Center
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.ExperimentalUnitApi
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.TextUnitType

@ExperimentalUnitApi
@ExperimentalAnimationApi
class NavigationGraphActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            JtCompose_DemoTheme {
            //dev
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    Greeting()
                }
            }
        }
    }
}

@ExperimentalUnitApi
@ExperimentalAnimationApi
@Composable
fun Greeting() {
    Box(
        Modifier
            .fillMaxSize()
            .background(Color.Black), contentAlignment = Center)
    {
        Card(
            backgroundColor = Color.DarkGray,
            modifier = Modifier
                .clip(RoundedCornerShape(8.dp))
                .border(2.dp, Color.LightGray, RoundedCornerShape(8.dp))
        ) {
            Card(
                modifier = Modifier.width(190.dp), shape = RoundedCornerShape(8.dp),
                elevation = 0.dp, backgroundColor = Color.DarkGray
            )
            {
                var expanded by remember { mutableStateOf(false) }
                Column(
                    Modifier
                        .clickable { expanded = !expanded }
                        .padding(10.dp),
                    horizontalAlignment = Alignment.CenterHorizontally,
                    verticalArrangement = Arrangement.Center)
                {
                    Image(
                        painter = painterResource(id = R.drawable.thewitcher),
                        contentDescription = "Hi",
                        contentScale = ContentScale.Crop,
                        modifier = Modifier
                            .size(104.dp)
                            .clip(CircleShape)
                            .border(2.dp, Color.Gray, CircleShape)
                    )
                    AnimatedVisibility(visible = expanded) {
                        Text(
                            text = buildAnnotatedString {
                                pushStyle(
                                    style = SpanStyle(
                                        color = Color.LightGray,
                                        fontFamily = FontFamily.Cursive
                                    )
                                )
                                append("The\n")
                                pushStyle(
                                    style = SpanStyle(
                                        color = Color.Gray,
                                        letterSpacing = TextUnit(0.2f, TextUnitType.Em),
                                        fontWeight = FontWeight.Bold
                                    )
                                )
                                append("Witcher!")
                            },
                            textAlign = TextAlign.Center,
                            style = MaterialTheme.typography.h4,
                            modifier = Modifier.padding(0.dp, 10.dp, 0.dp, 0.dp),
                        )
                    }
                }
            }
        }
    }
}

@ExperimentalUnitApi
@ExperimentalAnimationApi
@Preview(showBackground = false)
@Composable
fun DefaultPreview() {
    JtCompose_DemoTheme {
        Greeting()
    }
}
